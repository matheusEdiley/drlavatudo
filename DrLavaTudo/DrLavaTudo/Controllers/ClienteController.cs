﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;
using System.Web.Script.Serialization;

namespace DrLavaTudo.Controllers
{
    [RoutePrefix("api")]
    public class ClienteController : ApiController
    {
        [AcceptVerbs("POST")]
        [Route("clientes/create")]
        public string criarCliente(Cliente cli)
        {
            try
            {
                Repository<Cliente> c = new Repository<Cliente>(false);
                c.Add(cli);
                c.Commit();
                c.Dispose();
                return "200";
            }
            catch (Exception)
            {
                return "400";
            }
        }
    }
}

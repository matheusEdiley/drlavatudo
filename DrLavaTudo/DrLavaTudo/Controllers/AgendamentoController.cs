﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;
using System.Web.Script.Serialization;

namespace DrLavaTudo.Controllers
{
    [RoutePrefix("api")]
    public class AgendamentoController : ApiController
    {
        [AcceptVerbs("DELETE")]
        [Route("agendamento/{id_agendamento}")]
        public string deleteAgendamento(string id_agendamento)
        {
            try
            {
                Repository<Agendamento> a = new Repository<Agendamento>(false);
                a.Delete(x => x.CodAgend == int.Parse(id_agendamento));
                a.Commit();
                a.Dispose();

                return "200";
            }
            catch (Exception)
            {
                return "400";
            }
        }

        [AcceptVerbs("GET")]
        [Route("/tecnicos/{id_tecnico}/agendamentos/{data}")]
        public string agendamentoPorData(string id_tecnico, string data)
        {
            int codTecnico = int.Parse(id_tecnico);
            DateTime dataAgend = DateTime.Parse(data);

            Repository<Agendamento> a = new Repository<Agendamento>(false);
            var javaScriptSerializer = new JavaScriptSerializer();

            return javaScriptSerializer.Serialize(a.Get(x => x.CodTecnico == codTecnico
                && x.DatAgend == dataAgend).ToList());
        }
    }
}

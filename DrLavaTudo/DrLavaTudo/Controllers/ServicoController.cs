﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;
using System.Web.Script.Serialization;

namespace DrLavaTudo.Controllers
{
    [RoutePrefix("api")]
    public class ServicoController : ApiController
    {
        [AcceptVerbs("GET")]
        [Route("servicos/all")]
        public string servicosAll()
       {
            Repository<Servico> s = new Repository<Servico>(false);
            var javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Serialize(s.GetAll());
        }
    }
}

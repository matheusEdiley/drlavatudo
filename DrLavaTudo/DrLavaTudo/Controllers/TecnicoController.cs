﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;
using System.Web.Script.Serialization;

namespace DrLavaTudo.Controllers
{
    [RoutePrefix("api")]
    public class TecnicoController : ApiController
    {
        [AcceptVerbs("GET")]
        [Route("tecnicos/all")]
        public string tecnicosAll ()
        {
            Repository<Tecnico> t = new Repository<Tecnico>(false);
            var javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Serialize(t.GetAll());
        }
    }
}

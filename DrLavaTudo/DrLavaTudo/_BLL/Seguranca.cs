﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DrLavaTudo._BLL
{
    public class Seguranca
    {
        /// <summary>
        /// Mátodo auxiliar para remoção de caracteres maliciosos, possível SQL ou Script Injection
        /// </summary>
        /// <param name="Texto">Texto a ser verificado se existe parâmetros com palavras chaves maliciosas</param>
        /// <param name="MaxTexto">Tamanho máximo para um campo ou um parâmetro, padrão 50 caso não informado</param>
        /// <returns>Retorna o texto todo substituido pelas correções caso encontre alguma palavra lixo</returns>
        public static string ProtejeSqlInjection(string Texto, int MaxTexto = 50)
        {
            string TextoOK = null;
            if (Texto != null)
            {
                // Tamanho maximo de um parâmetro via URL definido
                if (Texto.Length > MaxTexto)
                    Texto = Texto.Substring(0, MaxTexto);

                // Vetor com as strings que serão consideradas perigosas para o Portal
                ArrayList Lixo = new ArrayList();
                Lixo.Add("'");
                Lixo.Add("''");
                Lixo.Add("DATABASE");
                Lixo.Add("SELECT");
                Lixo.Add("INSERT");
                Lixo.Add("DELETE");
                Lixo.Add("UPDATE");
                Lixo.Add("DROP");
                Lixo.Add("ALTER");
                Lixo.Add("TABLE");
                Lixo.Add("JAVA");
                Lixo.Add("SCRIPT");
                Lixo.Add(";");
                Lixo.Add("--");
                Lixo.Add("XP_");
                Lixo.Add("#");
                Lixo.Add("%");
                Lixo.Add("NULL");
                Lixo.Add("EMPTY");
                Lixo.Add("<");
                Lixo.Add(">");
                Lixo.Add("=");
                Lixo.Add("BODY");
                Lixo.Add("EMBED");
                Lixo.Add("FRAME");
                Lixo.Add("IMG");
                Lixo.Add("HTML");
                Lixo.Add("STYLE");
                Lixo.Add("LAYER");
                Lixo.Add("LINK");
                Lixo.Add("OBJECT");
                Lixo.Add("SESSION");
                Lixo.Add("APP");
                Lixo.Add("COOKIE");


                TextoOK = Texto.Trim();

                // Limpa a string conforme filtros do lixo. Não diferenciando maiúsculas e minúsculas
                foreach (string value in Lixo)
                {

                    TextoOK = Regex.Replace(TextoOK, value, "", RegexOptions.IgnoreCase);

                }

            }

            return TextoOK;

        }

        /// <summary>
        /// MÉTODO PARA VERIFICAR SE UMA STRING CONTEM APENAS CARACTERES PERMITIDOS
        /// </summary>
        /// <returns></returns>
        public static bool VerificarCaracteres(string value)
        {


            if (Regex.IsMatch(value, "^[A-Za-z0-9.,;?!&_:/ªº()*+-=/\\@#éúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄçÇ% ]*$"))
            {

                return true;

            }
            else
            {

                return false;

            }


        }

        /// <summary>
        /// Validação se uma string é um e-mail válido.
        /// </summary>
        /// <param name="Email">Alguma string.</param>
        /// <returns>Verdadeiro se é um e-mail válido, senão falso.</returns>
        public static bool ValidarEmail(string Email)
        {
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            if (!rg.IsMatch(Email))
                return false;

            return true;
        }

        /// <summary>
        /// MÉTODO PARA VALIDAR CPF
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public static bool ValidarCPF(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }
    }
}
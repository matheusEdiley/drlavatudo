﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Reflection;
using System.Collections;

namespace DrLavaTudo._BLL
{
    public static class MetodosAux
    {
        public enum ClasseMsg
        {
            Vermelho = 1,
            Amarelo = 2,
            Azul = 3,
            Verde = 4
        }

        /// <summary>
        /// Converte um DataTable em uma lista de objetos
        /// </summary>
        /// <typeparam name="T">Objeto generico</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>Lista de objetos do tipo especificado</returns>
        public static List<T> dataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                //Lista do objeto genérico
                List<T> list = new List<T>();
                //Percorre as linhas da tabela
                foreach (var record in table.AsEnumerable())
                {
                    T obj = new T(); //Instancia um objeto do tipo especificado

                    //Percorre a lista de atributos do objeto
                    foreach (var f in obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
                    {
                        try
                        {
                            //verificação para campos vazios
                            var value = !string.IsNullOrEmpty(record[f.Name].ToString()) ? record[f.Name] : null;
                            //Recuperar campo privado
                            FieldInfo fieldInfo = obj.GetType().GetField(f.Name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                            //Verificação para campos nullable
                            Type u = Nullable.GetUnderlyingType(fieldInfo.FieldType);
                            Type aux = fieldInfo.FieldType;

                            if (u != null) //Se for null o campo não é nullable
                            {
                                aux = u;
                            }

                            //Seta o valor do campo no objeto
                            fieldInfo.SetValue(obj, Convert.ChangeType((object)value, aux),
                                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, null);
                        }
                        catch (Exception)
                        {
                            //Caso ocorra algum erro, continua percorrendo a lista de atributos
                            continue;
                        }
                    }

                    //Adiciona o objeto preenchido na lista
                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
       
        /// <summary>
        /// MÉTODO PARA PREENCHER UMA MENSAGEM NA PÁGINA DE CONTROLE DE ATENDIMENTO
        /// </summary>
        /// <param name="titulo">TÍTULO DA MENSAGEM</param>
        /// <param name="msg">MENSAGEM</param>
        /// <param name="classe">Cor de fundo e texto da mensagem</param>
        /// <param name="pnl">Painel a ser preenchida a mensagem</param>
        /// <param name="focus">Define o foco no panel (bool - True or False)</param>
        /// <param name="clear">Indica se é para limpar o panel (true) ou adicionar nova mensagem, preservando a anterior</param>
        public static void PreencherMensagem(string msg, ClasseMsg classe, Panel pnl, bool focus, bool clear, string textolink,string link)
        {

            try
            {

                Dictionary<ClasseMsg, string> ClasseMsgStr = new Dictionary<ClasseMsg, string>(){
                    { ClasseMsg.Vermelho,"alert-danger"},
                    { ClasseMsg.Amarelo,"alert-warning"},
                    { ClasseMsg.Azul,"alert-info"},
                    { ClasseMsg.Verde,"alert-success"}
                };

                if (clear)
                {
                    //Limpa o panel
                    pnl.Controls.Clear();
                }

                //Se focus é true
                if (focus)
                {
                    //Define o cursor no panel (define o foco no panel)
                    pnl.Focus();
                }

                //Exibe a mensagem
                pnl.Controls.Add(new LiteralControl("<div class=\"panel-body\"><div class=\"alert "+ClasseMsgStr[classe]+" alert-dismissable\">"+
                               " <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + msg + " <a href=\"" + link +"\" class=\"alert-link\">" + textolink +"</a></div></div>"));

            }
            catch (Exception ex)
            {
            }

        }

        public static CultureInfo CultureInfo_Moeda()
        {

            CultureInfo info = new CultureInfo("pt-BR");

            info.NumberFormat.CurrencyDecimalSeparator = ",";
            info.NumberFormat.CurrencyGroupSeparator = ".";
            info.NumberFormat.CurrencySymbol = "R$";

            return info;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DrLavaTudo.Repository
{
    public class Repository<T> : IRepository<T>, IDisposable where T : class
    {
        private Models.Database1Entities1 Context;

        public Repository()
        {
            Context = new Models.Database1Entities1();
           
        }

        public Repository(bool flg)
        {
            Context = new Models.Database1Entities1();
            Context.Configuration.ProxyCreationEnabled = flg;
        }

        /// <summary>
        /// Lista todos os itens
        /// </summary>
        /// <returns>Retorna todos os objetos</returns>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        /// <summary>
        /// Lista uma coleção de objetos de acordo com os parametros
        /// </summary>
        /// <param name="predicate">Expressão lambda</param>
        /// <returns>Retorna uma coleção de objetos</returns>
        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return  Context.Set<T>().Where(predicate);
        }

        /// <summary>
        /// Procura um objeto dentro da coleção
        /// </summary>
        /// <param name="key">Array de parametros</param>
        /// <returns>Retorna objeto genérico</returns>
        public T Find(params object[] key)
        {
            return Context.Set<T>().Find(key);
        }
        
        /// <summary>
        /// Primeiro elemento da coleção que atende aos parametros
        /// </summary>
        /// <param name="predicate">Expressão lambda</param>
        /// <returns></returns>
        public T First(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate).FirstOrDefault();
        }
        /// <summary>
        /// Adiciona um objeto ao banco de dados
        /// </summary>
        /// <param name="entity">Objeto genérico</param>
        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }
        /// <summary>
        /// Edita um objeto no banco de dados
        /// </summary>
        /// <param name="entity">Objeto genérico</param>
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }
        /// <summary>
        /// Exclui um ou mais objetos do banco de dados.
        /// </summary>
        /// <param name="predicate">Expressão lambda</param>
        public void Delete(Func<T, bool> predicate)
        {
            Context.Set<T>()
           .Where(predicate).ToList()
           .ForEach(del => Context.Set<T>().Remove(del));
        }
       
        public void Commit()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
            }
            GC.SuppressFinalize(this);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Views/MasterPage.Master" AutoEventWireup="true" CodeBehind="_Tecnico.aspx.cs" Inherits="DrLavaTudo._Views._Tecnico" %>
<%@ MasterType 
    virtualpath="~/_Views/MasterPage.Master" 
%>
<asp:Content ID="Content1" ContentPlaceHolderID="cpHome" runat="server">
     <div class="container">
        <div class="centered title">
            <h1>Cadastro e consulta de técnicos</h1>
        </div>
    </div>
    <div class="row">

        <div class="col-md-4 col-md-offset-4">

            <fieldset>

                <!-- Form Name -->
                <legend>Preencha os dados</legend>


                <!-- Text input-->
                <div class="form-group">
                    <asp:Panel runat="server" ID="pnlMensagem"></asp:Panel>
                    <label class="col-sm-2 control-label" for="textinput">Nome</label>
                    <div class="col-sm-10">
                        <asp:TextBox class="form-control" runat="server" ID="txtNome"></asp:TextBox>

                    </div>
                </div>

                <br />
                <br />

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                            <asp:Button class="btn btn-primary" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" Text="Salvar" />
                            <asp:Button class="btn btn-default" runat="server" ID="btnLimpar" OnClick="btnLimpar_Click" Text="Limpar" />
                        </div>
                    </div>
                </div>

            </fieldset>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <hr />

    <div class="container">
        <div class="row">

            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default panel-table">
                  
                    <div class="panel-body">
                        <asp:GridView runat="server" ID="gvLista" OnRowCommand="gvLista_RowCommand" AutoGenerateColumns="false" 
                            DataKeyNames="CodTecnico, NomeTecnico" class="table table-striped table-bordered table-list">
                            <Columns>
                                <asp:ButtonField CommandName="excluir" ControlStyle-CssClass="btn btn-danger" Text="Excluir" />
                                <asp:ButtonField CommandName="editar" ControlStyle-CssClass="btn btn-primary" Text="Editar" />
                                <asp:BoundField HeaderText="Nome" DataField="NomeTecnico" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

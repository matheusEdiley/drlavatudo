﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Views/MasterPage.Master" AutoEventWireup="true" CodeBehind="_Home.aspx.cs" Inherits="DrLavaTudo._Views.Home" %>
<%@ MasterType 
    virtualpath="~/_Views/MasterPage.Master" 
%>

<asp:Content ID="Content2" ContentPlaceHolderID="cpHome" runat="server">

<div class="container">
        <div class="centered title">
            <h1>Bem vindo ao sistema Dr Lava tudo</h1>
        </div>
    </div>
    <div class="row">

        <div class="col-md-4 col-md-offset-4">

            <fieldset>

                <!-- Form Name -->
                <legend> Escolha uma das opções acima e comece a usar o sistema. </legend>
            </fieldset>
        </div>
    </div>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;

namespace DrLavaTudo._Views
{
    public partial class _Tecnico : System.Web.UI.Page
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            PreencherGrid();
        }

        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = int.Parse(e.CommandArgument.ToString());
                short codTecnico = short.Parse(gvLista.DataKeys[index][0].ToString());
                string nomeTecnico = gvLista.DataKeys[index][1].ToString();


                Repository<Models.Tecnico> r = new Repository<Models.Tecnico>();

                if (e.CommandName == "excluir")
                {
                    r.Delete(x => x.CodTecnico == codTecnico);
                    r.Commit();
                    r.Dispose();

                    MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
                    this.PreencherGrid();
                }
                else if (e.CommandName == "editar")
                {

                    Models.Tecnico c = new Models.Tecnico();
                    txtNome.Text = nomeTecnico;

                    Master.SessionTecnico = r.Get(x => x.CodTecnico == codTecnico).ToList()[0];
                    MetodosAux.PreencherMensagem("Editando Tecnico  nº: " + codTecnico, MetodosAux.ClasseMsg.Azul,
                       pnlMensagem, true, true, null, null);
                }

            }
            catch (Exception ex)
            {

                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Models.Tecnico c = new Models.Tecnico();

                if (!string.IsNullOrEmpty(txtNome.Text))
                {
                    c.NomeTecnico = txtNome.Text;
                }
                else
                {
                    MetodosAux.PreencherMensagem("Atenção, preeencha os campos corretamente", MetodosAux.ClasseMsg.Amarelo,
                        pnlMensagem, true, true, null, null);
                    return;
                }

                Repository<Models.Tecnico> r = new Repository<Models.Tecnico>();
                if (Master.SessionTecnico == null)
                {
                    r.Add(c);
                }
                else
                {
                    c.CodTecnico = Master.SessionTecnico.CodTecnico;
                    r.Update(Master.SessionTecnico);
                }

                r.Commit();
                r.Dispose();

                MetodosAux.PreencherMensagem("Salvo com sucesso!", MetodosAux.ClasseMsg.Verde,
                        pnlMensagem, true, true, null, null);
                this.PreencherGrid();

            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                       pnlMensagem, true, true, null, null);

            }
            finally
            {
                Master.SessionTecnico = null;
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            pnlMensagem.Controls.Clear();
            txtNome.Text = string.Empty;
            Master.SessionTecnico = null;
        }
        #endregion

        #region Métodos

        private void PreencherGrid()
        {
            try
            {
                Models.Tecnico s = new Models.Tecnico();
                Repository<Tecnico> r = new Repository<Tecnico>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Views/MasterPage.Master" AutoEventWireup="true" CodeBehind="_Pedido.aspx.cs" Inherits="DrLavaTudo._Views._Pedido" %>

<%@ MasterType
    VirtualPath="~/_Views/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHome" runat="server">
    <asp:ScriptManager runat="server" ID="sm"></asp:ScriptManager>

    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <h2>Pedidos <small>Cadastro e consulta de pedidos</small></h2>
                <asp:Panel runat="server" ID="pnlMensagem"></asp:Panel>
                <hr />
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <i class="fa fa-user"></i>&nbsp;Cliente
                        <div class="form-group">
                            <asp:DropDownList class="form-control" runat="server" TabIndex="1" ID="ddlCliente"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <i class="fa fa-bar-chart"></i>&nbsp; Status
                        <div class="form-group">
                            <asp:DropDownList class="form-control" runat="server" TabIndex="2" ID="ddlStatus"></asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <i class="fa fa-money "></i>&nbsp;Valor total
                        <div class="form-group">
                            <asp:TextBox runat="server" class="form-control" placeholder="R$ 0.00" TabIndex="5" ID="txtValor"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <i class="fa fa-credit-card"></i>&nbsp; Cartão de crédito?
                        <div class="form-group">
                            <asp:DropDownList class="form-control" OnSelectedIndexChanged="ddlCartao_SelectedIndexChanged"
                                AutoPostBack="true" runat="server" TabIndex="2" ID="ddlCartao">
                                <asp:ListItem Value="1">Sim</asp:ListItem>
                                <asp:ListItem Selected="True" Value="2">Não</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <asp:UpdatePanel runat="server" ID="upCartao">
                    <ContentTemplate>
                        <asp:Panel ID="pnlCartao" runat="server" Visible="false">

                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4 well well-sm" style="width: 47.5%">
                                        <legend><i class="fa fa-credit-card"></i>&nbsp;Informações do cartão</legend>

                                        <div class="row">
                                            <div class="col-xs-6 col-md-6">
                                                <asp:TextBox runat="server" class="form-control" placeholder="CPF Titular" ID="txtCPF"></asp:TextBox>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <asp:TextBox runat="server" class="form-control" placeholder="Número do cartão" ID="txtNumCartao"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-xs-6 col-md-6">
                                                <asp:TextBox runat="server" class="form-control" placeholder="Código de segurança" ID="txtCodSeg"></asp:TextBox>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <asp:TextBox runat="server" class="form-control" placeholder="Data de validade" ID="txtDatVal"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                            <asp:Button class="btn btn-success" runat="server" ID="btnSalvar"
                                OnClick="btnSalvar_Click" Text="Salvar" />
                            <asp:Button class="btn btn-default" runat="server" ID="btnLimpar" OnClick="btnLimpar_Click" Text="Limpar" />
                        </div>
                    </div>
                </div>
                <hr />

                <div class="panel-body">
                    <asp:GridView runat="server" ID="gvLista" OnRowCommand="gvLista_RowCommand" AutoGenerateColumns="false"
                        DataKeyNames="CodPed" class="table table-striped table-bordered table-list">
                        <Columns>
                            <asp:ButtonField CommandName="excluir" ControlStyle-CssClass="btn btn-danger" Text="Excluir" />
                            <asp:ButtonField CommandName="editar" ControlStyle-CssClass="btn btn-primary" Text="Editar" />
                            <asp:BoundField HeaderText="Cliente" DataField="Cliente.NomeCliente" />
                            <asp:BoundField HeaderText="Status" DataField="Status.DesStatus" />
                            <asp:BoundField HeaderText="Valor(R$)" DataField="Valor" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

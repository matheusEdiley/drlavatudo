﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Views/MasterPage.Master" AutoEventWireup="true" CodeBehind="_Agendamento.aspx.cs" Inherits="DrLavaTudo._Views._Agendamento" %>

<%@ MasterType
    VirtualPath="~/_Views/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHome" runat="server">
    <script type="text/javascript">
        $('.date').mask('00/00/0000');
    </script>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <h2>Agendamento <small>Cadastro e consulta de agendamentos e serviços</small></h2>
            <asp:Panel runat="server" ID="pnlMensagem"></asp:Panel>
            <hr />
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-paper-plane"></i>&nbsp;Pedido
                        <div class="form-group">
                            <asp:DropDownList class="form-control" runat="server" TabIndex="1" ID="ddlPedido"></asp:DropDownList>
                        </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-pencil-square-o"></i>&nbsp; Agendamento
                        <div class="form-group">
                            <asp:DropDownList class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAgendamento_SelectedIndexChanged" runat="server" TabIndex="1" ID="ddlAgendamento"></asp:DropDownList>
                        </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-cogs"></i>&nbsp;Serviço
                        <div class="form-group">
                            <asp:DropDownList class="form-control" runat="server" TabIndex="1" ID="ddlServico"></asp:DropDownList>
                        </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-suitcase"></i>&nbsp; Tecnico
                        <div class="form-group">
                            <asp:DropDownList class="form-control" runat="server" TabIndex="2" ID="ddlTecnico"></asp:DropDownList>
                        </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-calendar"></i>&nbsp;Data da visita
                        <div class="form-group">
                            <asp:TextBox runat="server" type="date" ID="txtData" class="form-control" placeholder="00/00/0000"></asp:TextBox>
                        </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <i class="fa fa-clock-o "></i>&nbsp; Hora
                        <div class="form-group">
                            <asp:TextBox runat="server" type="time" ID="txtHora" class="form-control" placeholder="00:00"></asp:TextBox>
                        </div>
                </div>
            </div>

            <hr />
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <asp:Button class="btn btn-success" runat="server" ID="btnSalvar"
                            OnClick="btnSalvar_Click" Text="Salvar" />
                        <asp:Button class="btn btn-default" runat="server" ID="btnLimpar" OnClick="btnLimpar_Click" Text="Limpar" />
                    </div>
                </div>
            </div>

            <br />
            <hr />

            <div class="panel-body">
                <asp:GridView runat="server" ID="gvLista" OnRowCommand="gvLista_RowCommand" AutoGenerateColumns="false"
                    DataKeyNames="CodAgend" class="table table-striped table-bordered table-list">
                    <Columns>
                        <asp:ButtonField CommandName="excluir" ControlStyle-CssClass="btn btn-danger" Text="Excluir" />
                        <asp:BoundField HeaderText="Status" DataField="DatAgend" />
                        <asp:BoundField HeaderText="Status" DataField="HoraAgend" />
                        <asp:BoundField HeaderText="Cliente" DataField="Pedido.Cliente.NomeCliente" />
                        <asp:BoundField HeaderText="Status" DataField="Servico.DesServ" />
                        <asp:BoundField HeaderText="Técnico" DataField="Tecnico.NomeTecnico" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

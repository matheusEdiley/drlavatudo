﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;
using DrLavaTudo.Controllers;

namespace DrLavaTudo._Views
{
    public partial class _Cliente : System.Web.UI.Page
    {

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            PreencherGrid();
        }

        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = int.Parse(e.CommandArgument.ToString());
                int codCliente = int.Parse(gvLista.DataKeys[index][0].ToString());
                string nomeCliente = gvLista.DataKeys[index][1].ToString();
               

                Repository<Models.Cliente> r = new Repository<Models.Cliente>();

                if (e.CommandName == "excluir")
                {
                    r.Delete(x => x.CodCliente == codCliente);
                    r.Commit();
                    r.Dispose();

                    MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
                    this.PreencherGrid();
                }
                else if (e.CommandName == "editar")
                {

                    Models.Cliente c = new Models.Cliente();
                    txtNome.Text = nomeCliente;

                    Master.SessionCliente = r.Get(x => x.CodCliente == codCliente).ToList()[0];
                    MetodosAux.PreencherMensagem("Editando Cliente  nº: " + codCliente, MetodosAux.ClasseMsg.Azul,
                       pnlMensagem, true, true, null, null);
                }

            }
            catch (Exception ex)
            {

                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Models.Cliente c = new Models.Cliente();

                if (!string.IsNullOrEmpty(txtNome.Text))
                {
                    c.NomeCliente = txtNome.Text;
                }
                else
                {
                    MetodosAux.PreencherMensagem("Atenção, preeencha os campos corretamente", MetodosAux.ClasseMsg.Amarelo,
                        pnlMensagem, true, true, null, null);
                    return;
                }

                Repository<Models.Cliente> r = new Repository<Models.Cliente>();
                if (Master.SessionCliente == null)
                {
                    //WebService
                    ClienteController cc = new ClienteController();
                    cc.criarCliente(c);
                }
                else
                {
                    c.CodCliente = Master.SessionCliente.CodCliente;
                    r.Update(c);
                    r.Commit();
                    r.Dispose();
                }

                MetodosAux.PreencherMensagem("Salvo com sucesso!", MetodosAux.ClasseMsg.Verde,
                        pnlMensagem, true, true, null, null);
                this.PreencherGrid();

            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                       pnlMensagem, true, true, null, null);

            }
            finally
            {
                Master.SessionCliente = null;
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            pnlMensagem.Controls.Clear();
            txtNome.Text = string.Empty;
            Master.SessionCliente = null;
        }
        #endregion

        #region Métodos

        private void PreencherGrid()
        {
            try
            {
                Models.Cliente s = new Models.Cliente();
                Repository<Cliente> r = new Repository<Cliente>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;

namespace DrLavaTudo._Views
{
    public partial class Servico : System.Web.UI.Page
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            PreencherGrid();
        }

        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = int.Parse(e.CommandArgument.ToString());
                short codServ = short.Parse(gvLista.DataKeys[index][0].ToString());
                string desServ = gvLista.DataKeys[index][1].ToString();
                short tempoServ = short.Parse(gvLista.DataKeys[index][2].ToString());

                Repository<Models.Servico> r = new Repository<Models.Servico>();

                if (e.CommandName == "excluir")
                {
                    r.Delete(x => x.CodServ == codServ);
                    r.Commit();
                    r.Dispose();

                    MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
                    this.PreencherGrid();
                }
                else if (e.CommandName == "editar")
                {

                    Models.Servico s = new Models.Servico();

                    txtDescricao.Text = desServ;
                    txtMinutos.Text = tempoServ.ToString();

                    Master.SessionServico = r.Get(x => x.CodServ == codServ).ToList()[0];
                    MetodosAux.PreencherMensagem("Editando Serviço  nº: " + codServ, MetodosAux.ClasseMsg.Azul,
                       pnlMensagem, true, true, null, null);
                }

            }
            catch (Exception ex)
            {

                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Models.Servico s = new Models.Servico();

                if (!string.IsNullOrEmpty(txtDescricao.Text))
                {
                    s.DesServ = txtDescricao.Text;
                }
                else
                {
                    MetodosAux.PreencherMensagem("Atenção, preeencha os campos corretamente", MetodosAux.ClasseMsg.Amarelo,
                        pnlMensagem, true, true, null, null);
                    return;
                }

                if (!string.IsNullOrEmpty(txtMinutos.Text))
                {
                    s.TempoServ = short.Parse(txtMinutos.Text);
                }
                else
                {
                    MetodosAux.PreencherMensagem("Atenção, preeencha os campos corretamente", MetodosAux.ClasseMsg.Amarelo,
                        pnlMensagem, true, true, null, null);
                    return;
                }

                Repository<Models.Servico> r = new Repository<Models.Servico>();
                if (Master.SessionServico == null)
                {
                    r.Add(s);
                }
                else
                {
                    s.CodServ = Master.SessionServico.CodServ;
                    r.Update(s);
                }

                r.Commit();
                r.Dispose();

                MetodosAux.PreencherMensagem("Salvo com sucesso!", MetodosAux.ClasseMsg.Verde,
                        pnlMensagem, true, true, null, null);
                this.PreencherGrid();

            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                       pnlMensagem, true, true, null, null);

            }
            finally
            {
                Master.SessionServico = null;
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            pnlMensagem.Controls.Clear();
            txtDescricao.Text = string.Empty;
            txtMinutos.Text = string.Empty;
        }
        #endregion

        #region Métodos

        private void PreencherGrid()
        {
            try
            {
                Models.Servico s = new Models.Servico();
                Repository<Models.Servico> r = new Repository<Models.Servico>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        #endregion
    
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;
using DrLavaTudo.Models;
using System.IO;
using DrLavaTudo.Controllers;

namespace DrLavaTudo._Views
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (false)
            {
                //Consumindo o webservice...
                var servicos = new ServicoController().servicosAll();
                var agendamentoPorData = new AgendamentoController().
                    agendamentoPorData("1", DateTime.Now.ToShortDateString());
                var deleteAgendamento = new AgendamentoController().deleteAgendamento("1");

                Cliente c = new Cliente();
                c.NomeCliente = "Teste WS";
                var createCliente = new ClienteController().criarCliente(c);

                var tecnicosAll = new TecnicoController().tecnicosAll();
            }
        }
    }
}
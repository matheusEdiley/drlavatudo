﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo.Models;
using DrLavaTudo._BLL;
using DrLavaTudo.Repository;

namespace DrLavaTudo._Views
{
    public partial class _Pedido : System.Web.UI.Page
    {

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    this.PreencherGrid();
                    this.AlimentarDDLs();
                }
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        //Determina se é pagamento com cartão
        protected void ddlCartao_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCartao.SelectedValue == "1") //Sim
                {
                    pnlCartao.Visible = true;
                }
                else if (ddlCartao.SelectedValue == "2") //Não
                {
                    pnlCartao.Visible = false;
                    txtCPF.Text = string.Empty;
                    txtNumCartao.Text = string.Empty;
                    txtDatVal.Text = string.Empty;
                    txtCodSeg.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        //Salvar pedido
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                int? codCadCartao = null;
                CadCartao cc = new CadCartao();
                if (ddlCartao.SelectedValue == "1") //Se o pagamento é feito no cartão, é necessário cadastro
                {
                    Repository<CadCartao> r = new Repository<CadCartao>();

                    codCadCartao = cc.CodCadCartao;
                    cc.CPFUsu = txtCPF.Text;
                    cc.NumCartao = txtNumCartao.Text;
                    cc.Validade = txtDatVal.Text;
                   
                    r.Add(cc);
                    r.Commit();
                    r.Dispose();

                    //Recuperar campo identity que acaba de ser inserido...
                    codCadCartao = cc.CodCadCartao;
                }
                

                Repository<Pedido> rPedido = new Repository<Pedido>();

                //Alimentando o objeto que será inserido
                Pedido p = new Pedido();
                p.CodCliente = int.Parse(ddlCliente.SelectedValue);
                p.CodStatus = short.Parse(ddlStatus.SelectedValue);
                p.Valor = float.Parse(txtValor.Text);
                p.CodCadCartao = codCadCartao;
                p.CodAtend = Master.AtendenteLogado.CodAtend;

                if (Master.SessionPedido == null) //Primeiro cadastro
                {
                    rPedido.Add(p);
                }
                else //Caso esteja em modo de edição
                {
                    p.CodPed = Master.SessionPedido.CodPed;
                    rPedido.Update(p);
                }

                rPedido.Commit();
                rPedido.Dispose();

                this.LimparCampos();
                MetodosAux.PreencherMensagem("Salvo com sucesso!", MetodosAux.ClasseMsg.Verde,
                        pnlMensagem, true, true, null, null);
                this.PreencherGrid();
            }
            catch (Exception ex)
            {
                this.LimparCampos();
                MetodosAux.PreencherMensagem("Erro: " + ex, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
            finally
            {
              
            }
        }

        //Limpa os campos
        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            this.LimparCampos();
        }

        //Editar e excluir registro
        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = int.Parse(e.CommandArgument.ToString());
                int codPed = int.Parse(gvLista.DataKeys[index][0].ToString());

                Repository<Models.Pedido> r = new Repository<Models.Pedido>();

                if (e.CommandName == "excluir")
                {
                    r.Delete(x => x.CodPed == codPed);
                    r.Commit();
                    r.Dispose();

                    MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
                    this.PreencherGrid();
                }
                else if (e.CommandName == "editar")
                {
                    Master.SessionPedido = (Pedido)r.Get(x => x.CodPed == codPed).ToList()[0];

                    ddlCartao.SelectedValue = (Master.SessionPedido.CodCadCartao == null) ? "2" : "1";
                    ddlStatus.SelectedValue = Master.SessionPedido.CodStatus.ToString();
                    ddlCliente.SelectedValue = Master.SessionPedido.CodCliente.ToString();

                    txtValor.Text = Master.SessionPedido.Valor.ToString();

                    if (Master.SessionPedido.CodCadCartao != null)
                    {
                        pnlCartao.Visible = true;

                        txtCPF.Text = Master.SessionPedido.CadCartao.CPFUsu;
                        txtNumCartao.Text = Master.SessionPedido.CadCartao.NumCartao;
                        txtCodSeg.Text = Master.SessionPedido.CadCartao.CodSeguranca.ToString();
                        txtDatVal.Text = Master.SessionPedido.CadCartao.Validade.ToString();
                    }

                    MetodosAux.PreencherMensagem("Editando Pedido  nº: " + codPed, MetodosAux.ClasseMsg.Azul,
                       pnlMensagem, true, true, null, null);
                }
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Alimenta DropDownLists  
        /// </summary>
        private void AlimentarDDLs()
        {
            try
            {
                Repository<Status> s = new Repository<Status>();
                ddlStatus.DataTextField = "DesStatus";
                ddlStatus.DataValueField = "CodStatus";
                ddlStatus.DataSource = s.GetAll().ToList();
                ddlStatus.DataBind();

                Repository<Cliente> c = new Repository<Cliente>();

                ddlCliente.DataTextField = "NomeCliente";
                ddlCliente.DataValueField = "CodCliente";

                ddlCliente.DataSource = c.GetAll().ToList();
                ddlCliente.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Limpar campos
        /// </summary>
        private void LimparCampos()
        {
            ddlCliente.ClearSelection();
            ddlStatus.ClearSelection();
            ddlCartao.SelectedValue = "2";

            txtValor.Text = string.Empty;
            txtCodSeg.Text = string.Empty;
            txtCPF.Text = string.Empty;
            txtDatVal.Text = string.Empty;
            txtNumCartao.Text = string.Empty;
            pnlCartao.Visible = false;

            Master.SessionPedido = null;

            pnlMensagem.Controls.Clear();
        }

        /// <summary>
        /// Preenche o grid com objetos
        /// </summary>
        private void PreencherGrid()
        {
            try
            {
                Models.Cliente s = new Models.Cliente();
                Repository<Pedido> r = new Repository<Pedido>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

    }
}
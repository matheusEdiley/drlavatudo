﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;

namespace DrLavaTudo._Views
{
    public partial class _Status : System.Web.UI.Page
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            PreencherGrid();
        }

        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = int.Parse(e.CommandArgument.ToString());
                short codStatus = short.Parse(gvLista.DataKeys[index][0].ToString());
                string nomeStatus = gvLista.DataKeys[index][1].ToString();


                Repository<Models.Status> r = new Repository<Models.Status>();

                if (e.CommandName == "excluir")
                {
                    r.Delete(x => x.CodStatus == codStatus);
                    r.Commit();
                    r.Dispose();

                    MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
                    this.PreencherGrid();
                }
                else if (e.CommandName == "editar")
                {

                    Models.Status c = new Models.Status();
                    txtDesStatus.Text = nomeStatus;

                    Master.SessionStatus = r.Get(x => x.CodStatus == codStatus).ToList()[0];
                    MetodosAux.PreencherMensagem("Editando Status  nº: " + codStatus, MetodosAux.ClasseMsg.Azul,
                       pnlMensagem, true, true, null, null);
                }

            }
            catch (Exception ex)
            {

                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Models.Status s = new Models.Status();

                if (!string.IsNullOrEmpty(txtDesStatus.Text))
                {
                    s.DesStatus = txtDesStatus.Text;
                }
                else
                {
                    MetodosAux.PreencherMensagem("Atenção, preeencha os campos corretamente", MetodosAux.ClasseMsg.Amarelo,
                        pnlMensagem, true, true, null, null);
                    return;
                }

                Repository<Models.Status> r = new Repository<Models.Status>();
                if (Master.SessionStatus == null)
                {
                    r.Add(s);
                }
                else
                {
                    s.CodStatus = Master.SessionStatus.CodStatus;
                    r.Update(s);
                }

                r.Commit();
                r.Dispose();

                MetodosAux.PreencherMensagem("Salvo com sucesso!", MetodosAux.ClasseMsg.Verde,
                        pnlMensagem, true, true, null, null);
                this.PreencherGrid();
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex.Message, MetodosAux.ClasseMsg.Vermelho,
                       pnlMensagem, true, true, null, null);

            }
            finally
            {
                Master.SessionStatus = null;
            }
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            pnlMensagem.Controls.Clear();
            txtDesStatus.Text = string.Empty;
        }
        #endregion

        #region Métodos

        private void PreencherGrid()
        {
            try
            {
                Models.Status s = new Models.Status();
                Repository<Status> r = new Repository<Status>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion
    }
}
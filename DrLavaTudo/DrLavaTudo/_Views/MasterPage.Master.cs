﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo.Models;
using DrLavaTudo.Repository;

namespace DrLavaTudo._Views
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        public Atendente AtendenteLogado
        {
            get { return (Atendente)Session["AtendenteLogado"]; }
            set { Session["AtendenteLogado"] = value; }
        }

        public Models.Agendamento SessionAgendamento
        {
            get { return (Models.Agendamento)Session["SessionAgendamento"]; }
            set { Session["SessionAgendamento"] = value; }
        }


        public Models.Servico SessionServico
        {
            get { return (Models.Servico)Session["SessionServico"]; }
            set { Session["SessionServico"] = value; }
        }

        public Models.Cliente SessionCliente
        {
            get { return (Models.Cliente)Session["SessionCliente"]; }
            set { Session["SessionCliente"] = value; }
        }

        public Models.Tecnico SessionTecnico
        {
            get { return (Models.Tecnico)Session["SessionTecnico"]; }
            set { Session["SessionTecnico"] = value; }
        }

        public Models.Status SessionStatus
        {
            get { return (Models.Status)Session["SessionStatus"]; }
            set { Session["SessionStatus"] = value; }
        }

        public Models.Pedido SessionPedido
        {
            get { return (Models.Pedido)Session["SessionPedido"]; }
            set { Session["SessionPedido"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Repository<Atendente> atendente = new Repository<Atendente>();
            var aux = atendente.Get(x => x.CodAtend == 1);
            this.AtendenteLogado = aux.ToArray()[0];
        }
    }
}
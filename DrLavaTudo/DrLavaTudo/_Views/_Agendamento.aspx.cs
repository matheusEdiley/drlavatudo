﻿using DrLavaTudo.Repository;
using DrLavaTudo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrLavaTudo._BLL;

namespace DrLavaTudo._Views
{
    public partial class _Agendamento : System.Web.UI.Page
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.AlimentarDDLs();
                this.PreencherGrid();
            }
        }
       
        //Salvar atendimento
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                #region Váriaveis
                int codPed = int.Parse(ddlPedido.SelectedValue);
                short codServ = short.Parse(ddlServico.SelectedValue);
                int codTecnico = int.Parse(ddlTecnico.SelectedValue);
                int codAgend = int.Parse(ddlAgendamento.SelectedValue);
                DateTime data = DateTime.Parse(txtData.Text);
                TimeSpan hora = TimeSpan.Parse(txtHora.Text);
                #endregion

                int somatorio = 0;
                Repository<Agendamento> p = new Repository<Agendamento>();

                if (p.Get(x => x.CodPed == codPed).Count() > 0) //Se já existe algum Agendamento para esse pedido...
                {
                    //Soma todos os tempos de serviços do pedido
                    somatorio = p.Get(x => x.CodPed == codPed).Sum(z => z.Servico.TempoServ);
                }
                
                //Soma o tempo total de servico do pedido com o tempo do serviço a ser cadastrado
                Repository<Models.Servico> s = new Repository<Models.Servico>();
                somatorio += s.Get(x => x.CodServ == codServ).ToList()[0].TempoServ;

                if (somatorio > 60) //Se o exceder 60 minutos não é permitido cadastrar mais serviços...
                {
                    MetodosAux.PreencherMensagem("Atenção: Esse serviço excede o tempo máximo por agendamento(60 minutos).",
                        MetodosAux.ClasseMsg.Amarelo, pnlMensagem, true, true, null, null);
                    return;
                }
                
                //Verifica se o proximo agendamento pra um determinado técnico não fere a regra de 1:30 horas de diferença entre cada agendamento
                Repository<Agendamento> a = new Repository<Agendamento>();
                var horaSub = hora.Subtract(TimeSpan.FromMinutes(90));
                var aux = a.Get(x => x.CodTecnico == codTecnico && x.DatAgend == data &&
                x.HoraAgend >= horaSub);

                if (aux.Count() > 0) //Se o tecnico já estiver ocupado, o agendamento não pode ser feito.
                {
                    MetodosAux.PreencherMensagem("Atenção: esse Técnico estará ocupado nos dia e hora especificados.",
                       MetodosAux.ClasseMsg.Amarelo, pnlMensagem, true, true, null, null);
                    return;
                }

                string msg = string.Empty;
                //Se o agendamento é novo...
                if (codAgend == 0)
                {
                    int? count = null;

                    var flg = a.Get(x => x.CodPed == codPed).Count();

                    if (flg == 0)
                    {
                        count = 1;
                    }
                    else
                    {
                        //Incrementa codAgend de acordo com o codPed
                        count = a.Get(x => x.CodPed == codPed).Max(z => z.CodAgend) + 1;
                    }

                    codAgend = (int)count;
                    msg = "Novo agendamento salvo com sucesso";
                }
                else
                {
                    msg = "Novo serviço salvo com sucesso no Agendamento Nº :" + codAgend;


                }
                //Alimenta o objeto que irá ser inserido
                Agendamento agend = new Agendamento();
                agend.CodAgend = codAgend;
                agend.CodPed = codPed;
                agend.CodServ = codServ;
                agend.CodTecnico = codTecnico;
                agend.DatAgend = data;
                agend.HoraAgend = hora;

                a.Add(agend);
                a.Commit();
                a.Dispose();

                MetodosAux.PreencherMensagem(msg, MetodosAux.ClasseMsg.Verde,
                       pnlMensagem, true, true, null, null);
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
            finally
            {
                this.AlimentarDDLs();
                Master.SessionAgendamento = null;
            }
        }
        //Limpar campos
        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            
            ddlPedido.ClearSelection();
            ddlServico.ClearSelection();
            ddlTecnico.ClearSelection();
            
            //Novo agendamento
            ddlAgendamento.SelectedValue = "0";

            txtData.Text = string.Empty;
            txtHora.Text = string.Empty;
        }
        //Muda o agendamento
        protected void ddlAgendamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int codAgend = int.Parse(ddlAgendamento.SelectedValue);

                if (codAgend == 0) //Em caso de Novo agendamento...
                {
                    
                    ddlPedido.ClearSelection();
                    ddlServico.ClearSelection();
                    ddlTecnico.ClearSelection();
                    txtData.Text = string.Empty;
                    txtHora.Text = string.Empty;

                    return;
                }

                //Caso não seja um novo agendamento, carrega as informações do agendamento solicitado
                Repository<Agendamento> a = new Repository<Agendamento>();
                var obj = a.Get(x => x.CodAgend == codAgend).ToList()[0]; //De acordo com o codAgend

                ddlPedido.SelectedValue = obj.CodPed.ToString();
                ddlServico.SelectedValue = obj.CodServ.ToString();
                ddlTecnico.SelectedValue = obj.CodTecnico.ToString();
                txtData.Text = obj.DatAgend.ToShortDateString();
                txtHora.Text = obj.HoraAgend.ToString();
            }
            catch (Exception ex)
            {
                MetodosAux.PreencherMensagem("Erro: " + ex, MetodosAux.ClasseMsg.Vermelho,
                      pnlMensagem, true, true, null, null);
            }
        }
        protected void gvLista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "excluir")
            {
                int index = int.Parse(e.CommandArgument.ToString());
                int codAgend = int.Parse(gvLista.DataKeys[index][0].ToString());

                Controllers.AgendamentoController a = new Controllers.AgendamentoController();
                a.deleteAgendamento(codAgend.ToString());

                MetodosAux.PreencherMensagem("Excluido com sucesso!", MetodosAux.ClasseMsg.Verde,
                      pnlMensagem, true, true, null, null);
                this.PreencherGrid();
                this.AlimentarDDLs();
            }
        }
        #endregion

        #region Métodos

        /// <summary>
        /// Alimenta DropDownLists
        /// </summary>
        private void AlimentarDDLs()
        {
            try
            {
                Repository<Pedido> p = new Repository<Pedido>();
                ddlPedido.DataTextField = "TxtPedido";
                ddlPedido.DataValueField = "CodPed";

                p.GetAll().ToList().ForEach(x => x.TxtPedido =
                $"[{x.Cliente.NomeCliente}] - [R$ { x.Valor }] - [{ x.Status.DesStatus }]");

                ddlPedido.DataSource = p.GetAll().ToList();
                ddlPedido.DataBind();

                Repository<Models.Servico> s = new Repository<Models.Servico>();
                ddlServico.DataTextField = "DesServ";
                ddlServico.DataValueField = "CodServ";
                ddlServico.DataSource = s.GetAll().ToList();
                ddlServico.DataBind();

                Repository<Tecnico> t = new Repository<Tecnico>();
                ddlTecnico.DataTextField = "NomeTecnico";
                ddlTecnico.DataValueField = "CodTecnico";
                ddlTecnico.DataSource = t.GetAll().ToList();
                ddlTecnico.DataBind();

                Repository<Agendamento> a = new Repository<Agendamento>();
                ddlAgendamento.DataTextField = "TxtAgendamento";
                ddlAgendamento.DataValueField = "CodAgend";

                a.GetAll().ToList().ForEach(x => x.TxtAgendamento =
                $"[{x.Pedido.Cliente.NomeCliente}] - [{ x.Servico.DesServ }] - [{ x.Tecnico.NomeTecnico}]");

                ddlAgendamento.DataSource = a.GetAll().ToList();
                ddlAgendamento.DataBind();

                ddlAgendamento.Items.Add(new ListItem("Novo Agendamento*", "0"));
                ddlAgendamento.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        /// <summary>
        /// Preenche o grid com objetos
        /// </summary>
        private void PreencherGrid()
        {
            try
            {
                Repository<Agendamento> r = new Repository<Agendamento>();
                gvLista.DataSource = r.GetAll().ToList();
                gvLista.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        
    }
}
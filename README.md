Teste - Dr Lava Tudo
=====================

Teste C# para vaga de desenvolvedor utilizando WebServices pra acesso ao banco de dados relacional.

## O projeto

O projeto deve ser aberto no Visual Studio, com o framework .NET 4.5.2, com suporte a SQL Server e C# 6.0.

- Abra o arquivo "DrLavaTudo.sln" utilizando o Visual Studio

ou digite o comandono terminal: 

```bash
$ devenv.exe DrLavaTudo.sln
```

## Notas sobre o projeto

- O banco de dados está no arquivo "DataBase1.mdf", na pasta App_Data.
- A imagem "Modelagem.jpg" mostra a estrutura do banco de dados. Tabelas, relacionamentos, chaves primarias e estrangeiras.
- Depois que abrir o VS, clique com o botão direito do mouse na página Home.aspx e clique em Set a Start Page
- Não foi criado sistema de login, porém na MasterPage é feito um carregamento da Session AtendenteLogado para simular o login
- O WebService está na pasta Controllers, e será consumido para fins de testes em suas respectivas páginas.
